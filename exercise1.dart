/*
M2-Assessment 1
Exercise 1
Write a basic program that stores and then prints the following data: 
Your name, favorite app, and city.
*/

void main()
{
  String name = "Dlozi Mthethwa";
  String favApp = "Twitter";
  String city = "Paris";
  print("My name is $name.");
  print("My favourite app to use is $favApp.");
  print("I would love to live in $city.");
}
/*M2-Assessment 1
Exercise 3
Create a class and 
a) then use an object to print the 
name of the app, sector/category, developer, and the year it won MTN Business App of the Year Awards. 

b) Create a function inside the class, 
transform the app name to all capital letters and then print the output.
*/


class MTNAppOfTheYear {
  List<Map<String, dynamic>> listOfWinners = [];
  
  MTNAppOfTheYear (List<Map<String, dynamic>> list)
  {
    listOfWinners = list;
  }

  void printAppName()
  {
    print("MTN Business App of the Year award winning apps");
    print("===============================================");
    listOfWinners.forEach((item) => print("${item['Year']} winner is ${item['Name']} in the ${item['Category']}"));
    print("");
  }

  void capitalize()
  {
    print("MTN Business App of the Year award winning apps");
    print("===============================================");
    listOfWinners.forEach((item) => print("${item['Year']} winner is ${item['Name'].toString().toUpperCase()} in the ${item['Category']}"));
    print("");
  }
}
void main()
{
  List<Map<String, dynamic>> appOfTheYear = [
  {"Year":"2021","Name":"Snapscan","Category":"Best Consumer Solution category"},
  {"Year":"2020","Name":"Wundrop","Category":"Best Enterprise Solution category"},
  {"Year":"2019","Name":"Live Inspect","Category":"Best African Solution category"},
  {"Year":"2018","Name":"Domestly","Category":"Best Gaming Solution category"},
  {"Year":"2017","Name":"Shytt","Category":"Best Health Solution category"},
  {"Year":"2016","Name":"Khula","Category":"Best Agricultural Solution category"},
  {"Year":"2015","Name":"Ecosystem","Category":"Best Educational Solution category"},
  {"Year":"2014","Name":"Naked Insurance","Category":"Best South African Solution category"},
  {"Year":"2013","Name":"EasyEquities","Category":"Best Campus Cup Solution category"},
  {"Year":"2012","Name":"EdTech","Category":"Huawei Category 15 category"}
  ];

  MTNAppOfTheYear winners = new MTNAppOfTheYear(appOfTheYear);
  winners.printAppName();
  winners.capitalize();
}


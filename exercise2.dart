/*
M2-Assessment 1
Exercise 2
Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012; 
a) Sort and print the apps by name;  
b) Print the winning app of 2017 and the winning app of 2018; 
c) the Print total number of apps from the array.
*/

void main()
{
  List<Map<String, dynamic>> appOfTheYear = [
  {"Year":"2021","Name":"Snapscan","Category":"Best Consumer Solution category"},
  {"Year":"2020","Name":"Wundrop","Category":"Best Enterprise Solution category"},
  {"Year":"2019","Name":"Live Inspect","Category":"Best African Solution category"},
  {"Year":"2018","Name":"Domestly","Category":"Best Gaming Solution category"},
  {"Year":"2017","Name":"Shytt","Category":"Best Health Solution category"},
  {"Year":"2016","Name":"Khula","Category":"Best Agricultural Solution category"},
  {"Year":"2015","Name":"Ecosystem","Category":"Best Educational Solution category"},
  {"Year":"2014","Name":"Naked Insurance","Category":"Best South African Solution category"},
  {"Year":"2013","Name":"EasyEquities","Category":"Best Campus Cup Solution category"},
  {"Year":"2012","Name":"EdTech","Category":"Huawei Category 15 category"}
  ];
  sortList(appOfTheYear);
  printAppName(appOfTheYear);
  printAppByYear(appOfTheYear,"2017");
  printAppByYear(appOfTheYear,"2018");
  printNumberOfApps(appOfTheYear);
}

void printAppByYear(List<Map<String, dynamic>> listOfWinners, String year)
{
  listOfWinners.forEach((item) => (item['Year']==year) ? print("The ${item['Year']} award winning app was ${item['Name']}\n") : null);
}

void printAppName(List<Map<String, dynamic>> listOfWinners)
{
  print("List of award winning apps");
  print("====================");
  listOfWinners.forEach((item) => print(item['Name']));
  print("");
}


void sortList(List<Map<String, dynamic>> listOfWinners)
{
  listOfWinners.sort((a, b) => a["Name"].compareTo(b["Name"]));
}

void printNumberOfApps(List<Map<String, dynamic>> listOfWinners)
{
  print("There are ${listOfWinners.length} apps in the list.\n");
}